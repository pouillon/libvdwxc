#ifndef VDW_Q0_H
#define VDW_Q0_H
void vdwxc_G_zeta0(double rs, double *e, double *dedn);
void vdwxc_compute_lda(double n, double* e, double* v);
void vdwxc_compute_lda_spin(double n_up, double n_dn,
                            double *e, double *v_up, double *v_dn);
void vdwxc_compute_lda_spin_zeta(double n, double zeta,
                                 double *e, double *v_up, double *v_dn);
void vdwxc_compute_q0x(double Z_ab, double rho, double sigma, double *q0x,
                       double *dq0x_drho, double *dq0x_dsigma);
void vdwxc_compute_q0x_spin(double Z_ab, double rho_up, double rho_dn,
                            double sigma_up, double sigma_dn,
                            double *q0xt,
                            double *dq0xt_drho_up, double *dq0xt_drho_dn,
                            double *dq0xt_dsigma_up, double *d0xt_dsigma_dn);
void vdwxc_compute_q0_spin(double Z_ab,
                           double rho_up, double rho_dn,
                           double sigma_up, double sigma_dn,
                           double* q0,
                           double* dq0drho_up, double* dq0drho_dn,
                           double* dq0dsigma_up, double* dq0dsigma_dn);
void vdwxc_hfilter(double q0, double q0cut, double *hq0, double *dhq0dq0);

void vdwxc_calculate_q0(int N, double Z_ab, double q_cut,
                        double* rho, double* sigma,
                        double* q0, double* dq0_drho,
                        double* dq0_dsigma);

// Now does the same as vdwxc_calculate_q0.  What to do?
void vdwxc_calculate_q0_nospin(int N, double Z_ab,
                               double *rho_g,
                               double *sigma_g,
                               double *q0_g,
                               double *rho_dq0drho_g,
                               double *rho_dq0dsigma_g);

// XXX q_cut
void vdwxc_calculate_q0_spin(int N, double Z_ab, //double q_cut,
                             double* rho_up_g, double* rho_dn_g,
                             double* sigma_up_g, double* sigma_dn_g,
                             double* q0_g,
                             double* dq0drho_gs,
                             double* dq0dsigma_gs);
#endif
