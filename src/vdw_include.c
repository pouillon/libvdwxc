/* This file is included from vdw_core once for each mode we want to
 compile, and must use ifdefs to define whatever mode-dependent
 behaviour we desire.  We have MODE_SERIAL, MODE_PFFT, and MODE_MPI. */

/* Note: make sure this file is *ALWAYS* included after config.h, as it relies
         on variables defined there. */


void METH(vdwxc_r2c)(vdwxc_data data)
{
    START_TIMER(r2c, "r2c");
#ifdef MODE_PFFT
#ifdef HAVE_PFFT
    pfft_execute(data->pfft_plan_r2c);
#else
    assert(0);
#endif
#else
    fftw_execute_dft_r2c(data->plan_r2c, (double*)data->work_ka, data->work_ka);
#endif
    END_TIMER(r2c, "r2c");
}

void METH(vdwxc_c2r)(vdwxc_data data)
{
    START_TIMER(c2r, "c2r");
#ifdef MODE_PFFT
#ifdef HAVE_PFFT
    pfft_execute(data->pfft_plan_c2r);
#else
    assert(0);
#endif
#else
    fftw_execute_dft_c2r(data->plan_c2r, data->work_ka, (double*)data->work_ka);
#endif
    END_TIMER(c2r, "c2r");
}

// Calculate Fourier-space convolution of theta_ka with kernel to get energy and F_ka.
double METH(vdwxc_convolution)(vdwxc_data data)
{
    START_TIMER(convolution, "convolution");
    int N0, N1, N2;
    double *kernel_aa = (double*)malloc(sizeof(double) * data->kernel.nalpha*data->kernel.nalpha);
    // work_ka: on input will be theta_ka.  On output will be F_ka.
    double complex* work_ka = (complex double*)data->work_ka;
    double energy = 0.0;
    int a1,a2;
    complex double F_a[data->kernel.nalpha];

    int N1glob, N0glob, N2glob;
    int kindex;

    // The nesting order depends on which parallelization mode we use.
    // Thus we define it differently for each.
#ifdef MODE_SERIAL
    for(N0 = 0; N0 < data->icell.Nlocal[0]; N0++) {
        N0glob = N0 + data->icell.offset[0];
        for(N1 = 0; N1 < data->icell.Nlocal[1]; N1++) {
            N1glob = N1 + data->icell.offset[1];
            for (N2 = 0; N2 < data->icell.Nlocal[2]; N2++) {
                N2glob = N2 + data->icell.offset[2];
                kindex = N2 + data->icell.Nlocal[2] * (N1 + data->icell.Nlocal[1] * N0);
#endif
#ifdef MODE_MPI
    for(N1 = 0; N1 < data->icell.Nlocal[1]; N1++) {
        N1glob = N1 + data->icell.offset[1];
        for(N0 = 0; N0 < data->icell.Nlocal[0]; N0++) {
            N0glob = N0 + data->icell.offset[0];
            for (N2 = 0; N2 < data->icell.Nlocal[2]; N2++) {
                N2glob = N2 + data->icell.offset[2];
                kindex = N2 + data->icell.Nlocal[2] * (N0 + data->icell.Nlocal[0] * N1);
#endif
#ifdef MODE_PFFT
    for(N1 = 0; N1 < data->icell.Nlocal[1]; N1++) {
        N1glob = N1 + data->icell.offset[1];
        for (N2 = 0; N2 < data->icell.Nlocal[2]; N2++) {
            N2glob = N2 + data->icell.offset[2];
            for(N0 = 0; N0 < data->icell.Nlocal[0]; N0++) {
                N0glob = N0 + data->icell.offset[0];
                kindex = N0 + data->icell.Nlocal[0] * (N2 + data->icell.Nlocal[2] * N1);
#endif
#ifdef THIS_NEVER_HAPPENS
                assert(0);
// This clause should never be reached.  We take the opportunity to close the
// "extraneous" opening braces above so Emacs and friends accept indentation.  So beautiful.
}}}}}}
#endif
                int weight = (N2glob == 0 || N2glob == data->icell.Nglobal[2] - 1) ? 1 : 2;
                int k0 = ((N0glob + data->cell.Nglobal[0] / 2) % data->cell.Nglobal[0]) -
                    data->cell.Nglobal[0] / 2;
                int k1 = ((N1glob + data->cell.Nglobal[1] / 2) % data->cell.Nglobal[1]) -
                    data->cell.Nglobal[1] / 2;
                int k2 = ((N2glob + data->cell.Nglobal[2] / 2) % data->cell.Nglobal[2]) -
                    data->cell.Nglobal[2] / 2;
                double kx = data->icell.vec[0] * k0 +
                            data->icell.vec[3] * k1 +
                            data->icell.vec[6] * k2;
                double ky = data->icell.vec[1] * k0 +
                            data->icell.vec[4] * k1 +
                            data->icell.vec[7] * k2;
                double kz = data->icell.vec[2] * k0 +
                            data->icell.vec[5] * k1 +
                            data->icell.vec[8] * k2;
                double k = sqrt(kx * kx + ky * ky + kz * kz);
                // Interpolate the kernels to 2D-array
                vdwxc_interpolate_kernels(&data->kernel, k, kernel_aa);
                double* ikernel_aa = kernel_aa;
                for (a1=0; a1 < data->kernel.nalpha; a1++) {
                    double complex F = 0.0;
                    for (a2=0; a2 < data->kernel.nalpha; a2++) {
                        // This array should be completely contiguous and needs no fancy indexing.
                        double complex a2val = work_ka[kindex * data->kernel.nalpha + a2];
                        double kernel = *ikernel_aa++;
                        double complex dF = a2val * kernel;
                        F += dF;
                    }
                    //assert(kindex < data->icell.Nlocal[0]*data->icell.Nlocal[1]*data->icell.Nlocal[2]);
                    double complex a1val = conj(work_ka[kindex * data->kernel.nalpha + a1]);
                    energy += weight * (creal(a1val) * creal(F) - cimag(a1val) * cimag(F));
                    F_a[a1] = F;
                }
                for(a1=0; a1 < data->kernel.nalpha; a1++) {
                    work_ka[kindex * data->kernel.nalpha + a1] = F_a[a1];
                }
            }
        }
    }
    energy *= 0.5 * data->cell.dV / data->Nglobal;
    free(kernel_aa);
    END_TIMER(convolution, "convolution");
    return energy;
}

// Calculate van der Waals energy plus energy/density derivative and
// energy/gradient-density derivative from density and gradient.
double METH(vdwxc_calculate)(vdwxc_data data)
{
    START_TIMER(vdwxc_calculate, "calculate");
    vdwxc_q0_and_theta(data);
#if VDWXC_DEBUG
    vdwxc_write_workbuffer(data, "libvdwxc.theta_ga.dat");
#endif
    METH(vdwxc_r2c)(data);
#if VDWXC_DEBUG
    vdwxc_write_workbuffer(data, "libvdwxc.theta_ka.dat");
#endif
    double energy = METH(vdwxc_convolution)(data);
#if VDWXC_DEBUG
    vdwxc_write_workbuffer(data, "libvdwxc.F_ka.dat");
#endif
    METH(vdwxc_c2r)(data);
#if VDWXC_DEBUG
    vdwxc_write_workbuffer(data, "libvdwxc.F_ga.dat");
#endif
    vdwxc_potential(data);
    END_TIMER(vdwxc_calculate, "calculate");
    return energy;
}
