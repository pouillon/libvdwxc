#include <stdio.h>
#include <assert.h>

#if defined HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef HAVE_MPI
#include <mpi.h>
#include "vdwxc_mpi.h"
#else
#include "vdwxc.h"
#endif


void vdwxc_new_(int* functional, vdwxc_data* vdw)
{
    vdwxc_data data = vdwxc_new(*functional);
    *vdw = data;
}

void vdwxc_new_spin_(int* functional, vdwxc_data* vdw)
{
    vdwxc_data data = vdwxc_new_spin(*functional);
    *vdw = data;
}

void vdwxc_print_(vdwxc_data* vdw)
{
    vdwxc_print(*vdw);
}

void vdwxc_set_unit_cell_(vdwxc_data* vdw, int* Nx, int* Ny, int* Nz,
                          double* C00, double* C01, double* C02,
                          double* C10, double* C11, double* C12,
                          double* C20, double* C21, double* C22)
{
    vdwxc_set_unit_cell(*vdw, *Nx, *Ny, *Nz,
                        *C00, *C01, *C02,
                        *C10, *C11, *C12,
                        *C20, *C21, *C22);
}

void vdwxc_init_serial_(vdwxc_data* vdw)
{
    vdwxc_init_serial(*vdw);
}

#ifdef HAVE_MPI
void vdwxc_init_mpi_(vdwxc_data* vdw, MPI_Fint* mpi_comm)
{
    MPI_Comm c_comm = MPI_Comm_f2c(*mpi_comm);
    vdwxc_init_mpi(*vdw, c_comm);
}

#ifdef HAVE_PFFT
void vdwxc_init_pfft_(vdwxc_data* vdw, MPI_Fint* mpi_comm, int* nx, int* ny)
{
    MPI_Comm c_comm = MPI_Comm_f2c(*mpi_comm);
    vdwxc_init_pfft(*vdw, c_comm, *nx, *ny);
}
#endif
#endif

void vdwxc_calculate_(vdwxc_data* vdw, double** rho_g, double** sigma_g,
                      double** dedn_g, double** dedsigma_g, double* energy)
{
    *energy = vdwxc_calculate(*vdw, *rho_g, *sigma_g, *dedn_g, *dedsigma_g);
}

void vdwxc_calculate_spin_(vdwxc_data* vdw, double **rho_up_g, double **rho_dn_g,
                           double **sigma_up_g, double **sigma_dn_g,
                           double **dedn_up_g, double **dedn_dn_g,
                           double **dedsigma_up_g, double **dedsigma_dn_g,
                           double *energy)
{
    *energy = vdwxc_calculate_spin(*vdw, *rho_up_g, *rho_dn_g,
                                   *sigma_up_g, *sigma_dn_g,
                                   *dedn_up_g, *dedn_dn_g,
                                   *dedsigma_up_g, *dedsigma_dn_g);
}


void vdwxc_finalize_(vdwxc_data* vdw)
{
    vdwxc_finalize(vdw);
}
