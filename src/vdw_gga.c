#include <math.h>

#include "vdwxc.h"

#if defined HAVE_CONFIG_H
#include "config.h"
#endif

#include "vdw_core.h"


/* We need to implement:

   * PBE R exchange
   * GGA-RPW86 exchange
   * Langreth-Vosko thing with interpolation to GGA-RPW86-X
   * LDA-PW correlation

 */

void vdwxc_lda_correlation(int Ng, double* rho_g, double* eps_g, double* dedn_g)
{
    double
        C0I = 0.238732414637843,
        gamma = 0.031091,
        alpha1 = 0.21370,
        beta1 = 7.5957,
        beta2 = 3.5876,
        beta3 = 1.6382,
        beta4 = 0.49294;  // defined also in vdwxc.h... use them from there.

    int i;
    for(i=0; i < Ng; i++) {
        double rtrs = pow(C0I / (*rho_g), 1.0 / 6.0);
        double rs = rtrs * rtrs;
        double Q0 = -2.0 * gamma * (1.0 + alpha1 * rtrs * rtrs);
        double Q1 = 2.0 * gamma * rtrs *
            (beta1 + rtrs * (beta2 + rtrs * (beta3 + rtrs * beta4)));
        double G1 = Q0 * log(1.0 + 1.0 / Q1);

        double dQ1drs = gamma * (beta1 / rtrs + 2.0 * beta2 +
                                 rtrs * (3.0 * beta3 + 4.0 * beta4 * rtrs));
        double dGdrs = -2.0 * gamma * alpha1 * G1 / Q0 - Q0
            * dQ1drs / (Q1 * (Q1 + 1.0));

        //double ec = G1;
        //double decdrs_0 = dGdrs;
        *eps_g += (*rho_g) * G1;
        *dedn_g += G1 - rs * dGdrs / 3.0;

        rho_g++;
        eps_g++;
        dedn_g++;
    }
}


// sigma = || grad rho ||^2
void vdwxc_cx_semilocal_exchange(int Ng, double* rho_g, double* sigma_g,
                                 double* eps_g, double* dedn_g, double* dedsigma_g)
{
    //double tol = 1e-20;

    double alp = 0.021789;
    double beta = 1.15;
    double a = 1.851;
    double b = 17.33;
    double c = 0.163;
    double mu_LM = 0.09434;
    double s_prefactor = 6.18733545256027;
    double Ax = -0.738558766382022; // = -3./4. * (3./pi)**(1./3)

    int i;
    for(i=0; i < Ng; i++) {
        double grad_rho = sqrt(*sigma_g);

        // eventually we need s to power 12.  Clip to avoid overflow
        // (We have individual tolerances on both rho and grho, but
        // they are not sufficient to guarantee this)
        double rho_pow_1_3 = pow(*rho_g, 1.0 / 3.0);
        double rho_pow_4_3 = (*rho_g) * rho_pow_1_3;
        double s_1 = grad_rho / (s_prefactor * rho_pow_4_3);
        s_1 = s_1 < 0.0 ? 0.0 : (s_1 > 1e20 ? 1e20 : s_1);
        double s_2 = s_1 * s_1;
        double s_3 = s_2 * s_1;
        double s_4 = s_3 * s_1;
        double s_5 = s_4 * s_1;
        double s_6 = s_5 * s_1;

        double fs_rPW86 = pow(1.0 + a * s_2 + b * s_4 + c * s_6, 1. / 15.);

        double fs = (1.0 + mu_LM * s_2) / (1.0 + alp * s_6)
            + alp * s_6 / (beta + alp * s_6) * fs_rPW86;

        // the energy density for the exchange.
        *eps_g += Ax * rho_pow_4_3 * fs;

        double df_rPW86_ds = (1. / (15. * pow(fs_rPW86, 14.0))) *
            (2.0 * a * s_1 + 4.0 * b * s_3 + 6.0 * c * s_5);

        double tmp = 1. + alp * s_6;
        double df_ds = 1. / (tmp * tmp)
            * (2.0 * mu_LM * s_1 * (1. + alp * s_6)
               - 6.0 * alp * s_5 * (1. + mu_LM * s_2))
            + alp * s_6 / (beta + alp * s_6) * df_rPW86_ds
            + 6.0 * alp * s_5 * fs_rPW86 / (beta + alp * s_6)
            * (1. - alp * s_6 / (beta + alp * s_6));

        // de/dn.  This is the partial derivative of sx wrt. n, for s constant
        *dedn_g += Ax * (4.0 / 3.0) *
            (rho_pow_1_3 * fs - grad_rho * df_ds / (s_prefactor * (*rho_g)));
        // de/d(nabla n).  The other partial derivative
        *dedsigma_g += 0.5 * Ax * df_ds / (s_prefactor * grad_rho);
        // (We may or may not understand what that grad_rho is doing here.)
        rho_g++;
        sigma_g++;
        eps_g++;
        dedn_g++;
        dedsigma_g++;
    }
}

void vdwxc_add_cx_exchange(vdwxc_data data, double* rho_g, double* sigma_g,
                           double* eps_g, double* dedn_g, double* dedsigma_g)
{
    vdwxc_cx_semilocal_exchange(data->Ng, rho_g, sigma_g, eps_g, dedn_g, dedsigma_g);
}
