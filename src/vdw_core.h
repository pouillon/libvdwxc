/* PFFT headers include FFTW-MPI, and FFTW-MPI headers include FFTW and MPI.
   No matter what, we only need to include one file.

   This header is internal.  We do not bother to have a serial and parallel version.
*/

#ifndef VDW_CORE_H
#define VDW_CORE_H

#include "vdwxc.h"
#include "vdw_kernel.h"

#if defined HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef HAVE_PFFT
#include <pfft.h>
#else
#ifdef HAVE_MPI
#include <fftw3-mpi.h>
#else
#include <fftw3.h>
#endif
#endif

#define VDWXC_PI 3.14159265358979323846

void vdwxc_writefile(char* name, int N, double *array);


struct vdwxc_unit_cell
{
    double vec[9];
    int Nglobal[3];
    int Nlocal[3];
    int offset[3];
    double dV;
};


struct vdwxc_data_obj
{
    int initialized;
    int functional;
    int nspin;
    double Z_ab;
    double q_cut;
    struct vdwxc_unit_cell cell;
    struct vdwxc_unit_cell icell;
    struct vdwxc_kernel kernel;

    int fft_type;
#ifdef HAVE_MPI
    MPI_Comm mpi_comm;
#endif

    int mpi_rank;
    int mpi_size;

    int Ng; // Number of local real space points
    int Nglobal; // Number of global real space points
    int gLDA;

    int pfft_grid[2];

    double* q0_g;
    double complex* work_ka; // theta_ga (padded), theta_ka, and F_ka.

    //int Nk; // Number of local reciprocal space points
    int kLDA; // Shortest dimension of k stored in reduced r2c-format.

    double* rho_dq0drho_sg;
    double* rho_dq0dsigma_sg;

    fftw_plan plan_r2c;
    fftw_plan plan_c2r;
    //fftw_plan radial_plan_r2c;
    //fftw_plan radial_plan_c2r;

    // Now some non-allocated (convenience) pointers.
    // These pointers will be used/set during a calculation, but do not
    // involve (de)allocation of any arrays.  They do save us from having
    // to pass all those arrays as arguments though.
    double *rho;
    double *rho_up;
    double *rho_dn;
    double *sigma;
    double *sigma_up;
    double *sigma_dn;
    double *dedn;
    double *dedn_up;
    double *dedn_dn;
    double *dedsigma;
    double *dedsigma_up;
    double *dedsigma_dn;
    // End of convenience pointers

#ifdef HAVE_PFFT
    pfft_plan pfft_plan_r2c;
    pfft_plan pfft_plan_c2r;
    MPI_Comm mpi_cart_comm;
#endif
    int errorcode;
};


#define VDWXC_LDA_A 0.031091
#define VDWXC_LDA_a1 0.2137
#define VDWXC_LDA_b1 7.5957
#define VDWXC_LDA_b2 3.5876
#define VDWXC_LDA_b3 1.6382
#define VDWXC_LDA_b4 0.49294

#define VDWXC_KF_PREFACTOR 3.0936677262801355 // (3.0 * pi**2)**(1.0 / 3.0)
#define VDWXC_RS_PREFACTOR 0.6203504908994001 // (3.0 / (4.0 * pi))**(1.0 / 3.0)

#define VDWXC_ERROR_NOERROR 0
#define VDWXC_ERROR_UNINITIALIZED 1
#define VDWXC_ERROR_UNKNOWN_FUNCTIONAL 2
#define VDWXC_ERROR_UNIT_CELL_NOT_SET 3
#define VDWXC_ERROR_UNIT_CELL_ALREADY_SET 4
#define VDWXC_ERROR_RADIAL_GRID_NOT_SET 5

#define VDWXC_INITIALIZED (unsigned int) 0x76647778 // vdwx
#define VDWXC_UNINITIALIZED 0

//void vdwxc_q0_and_theta(vdwxc_data data);
//void vdwxc_fft_r2c(vdwxc_data data);
//void vdwxc_fft_c2r(vdwxc_data data);
//void vdwxc_potential(vdwxc_data data);
//void vdwxc_fatal(char* msg);
//void vdwxc_nullify_convenience_pointers(vdwxc_data data);
void vdwxc_allocate_buffers(vdwxc_data data);

//double vdwxc_calculate_anyspin(vdwxc_data data);

#ifdef HAVE_MPI
void vdwxc_set_communicator(vdwxc_data data, MPI_Comm mpi_comm);
#endif

#endif
