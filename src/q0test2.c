#include <stdio.h>
#include <assert.h>
#include <math.h>
#include "vdw_q0.h"

int main(int argc, char** argv)
{
    double rs = 0.5;
    double e, dedrs;
    vdwxc_G_zeta0(rs, &e, &dedrs);
    double e1, e2, tmp;
    double drs = 1e-8;
    vdwxc_G_zeta0(rs - drs, &e1, &tmp);
    vdwxc_G_zeta0(rs + drs, &e2, &tmp);
    double deriv = 0.5 * (e2 - e1) / drs;
    double deriv_err = fabs(deriv - dedrs);
    printf("G_pw e=%.16f dedrho=%f deriv_err=%e\n\n", e, dedrs, deriv_err);
    assert(fabs(-0.0949608976455678 - e) < 1e-15);
    assert(deriv_err < 1e-9);

    double rho_up = 0.85, rho_dn = 0.35;
    double rho = rho_up + rho_dn;
    double zeta = (rho_up - rho_dn) / rho;

    double eps, v;
    vdwxc_compute_lda(rho, &eps, &v);
    eps *= rho;
    double ref = -0.0872542585762377;
    double err = fabs(ref - eps);

    double drho = 1e-8;
    vdwxc_compute_lda(rho - drho, &e1, &tmp);
    vdwxc_compute_lda(rho + drho, &e2, &tmp);
    e1 *= rho - drho;
    e2 *= rho + drho;
    double lda_deriv = 0.5 * (e2 - e1) / drho;
    //printf("lda spinpair deriv
    double lda_deriv_err = fabs(lda_deriv - v);
    printf("lda spinpair\neps=%.16f err=%e\nv=%.11f deriv=%.11f deriv_err=%e\n\n",
           eps, err, v, lda_deriv, lda_deriv_err);
    assert(err < 1e-15);
    assert(lda_deriv_err < 1e-8);

    //double zeta = 0.7;
    double eps_spin;
    double v_up, v_dn;
    double eps_spin_ref = -0.0821360927715360;
    vdwxc_compute_lda_spin_zeta(rho, zeta, &eps_spin, &v_up, &v_dn);
    eps_spin *= rho;
    //eps_spin *= rho;
    double e_err = fabs(eps_spin_ref - eps_spin);
    printf("lda spinzeta\neps=%.11f err=%e v_up=%.11f v_dn=%.11f\n\n",
           eps_spin, e_err, v_up, v_dn);
    assert(e_err < 1e-15);

    vdwxc_compute_lda_spin(rho_up, rho_dn, &eps_spin, &v_up, &v_dn);
    eps_spin *= rho;

    double espin1, espin2;
    vdwxc_compute_lda_spin(rho_up - drho, rho_dn, &espin1, &tmp, &tmp);
    vdwxc_compute_lda_spin(rho_up + drho, rho_dn, &espin2, &tmp, &tmp);
    espin1 *= rho - drho;
    espin2 *= rho + drho;

    double deriv_spin_up = 0.5 * (espin2 - espin1) / drho;
    double deriv_err_up = fabs(deriv_spin_up - v_up);
    double err_spin2 = fabs(eps_spin_ref - eps_spin);
    printf("lda spin\n");
    printf("eps=%.16f err=%e\n", eps_spin, err_spin2);
    printf("v_up=%.16f v_dn=%.16f\n", v_up, v_dn);
    assert(err_spin2 < 1e-15);
    vdwxc_compute_lda_spin(rho_up, rho_dn - drho, &espin1, &tmp, &tmp);
    vdwxc_compute_lda_spin(rho_up, rho_dn + drho, &espin2, &tmp, &tmp);
    espin1 *= rho - drho;
    espin2 *= rho + drho;

    double deriv_spin_dn = 0.5 * (espin2 - espin1) / drho;
    double deriv_err_dn = fabs(deriv_spin_dn - v_dn);
    printf("err_up=%e err_dn=%e\n\n", deriv_err_up, deriv_err_dn);
    assert(deriv_err_up < 1e-8);
    assert(deriv_err_dn < 1e-8);

    double Z_ab = -75.0;
    double sigma = 0.3;
    double dsigma = drho;
    double q0, q0_1, q0_2, dq0drho, dq0dsigma;
    double q0x_ref = 3.419535675401194;
    vdwxc_compute_q0x(Z_ab, rho, sigma, &q0, &dq0drho, &dq0dsigma);

    vdwxc_compute_q0x(Z_ab, rho - drho, sigma, &q0_1, &tmp, &tmp);
    vdwxc_compute_q0x(Z_ab, rho + drho, sigma, &q0_2, &tmp, &tmp);
    double dq0drho_numeric = 0.5 * (q0_2 - q0_1) / drho;
    double dq0drho_err = fabs(dq0drho_numeric - dq0drho);

    vdwxc_compute_q0x(Z_ab, rho, sigma - dsigma, &q0_1, &tmp, &tmp);
    vdwxc_compute_q0x(Z_ab, rho, sigma + dsigma, &q0_2, &tmp, &tmp);
    double dq0dsigma_numeric = 0.5 * (q0_2 - q0_1) / dsigma;
    double dq0dsigma_err = fabs(dq0dsigma_numeric - dq0dsigma);

    double q0x_err = fabs(q0x_ref - q0);
    printf("q0x %.15f err %e\n", q0, q0x_err);
    printf("dq0drho=%f err=%e\n", dq0drho, dq0drho_err);
    printf("dq0dsigma=%f err=%e\n\n", dq0dsigma, dq0dsigma_err);
    assert(q0x_err < 1e-14);
    assert(dq0drho_err < 5e-8);
    assert(dq0dsigma_err < 2e-8);
    //double ref2 = -0.0650794082170656;
    //double err2 = fabs(ref2 - eps2);
    //printf("eps2=%.16f, err=%e\n", eps2, err2);
    //assert(err2 < 1e-15);

    //double rho_up = 2.2, n_dn = 0.9;
    double sigma_up = 1.5, sigma_dn = 0.75;
    double q0xt, dq0xt_drho_up, dq0xt_drho_dn,
        dq0xt_dsigma_up, dq0xt_dsigma_dn;
    Z_ab = -25.0;
    vdwxc_compute_q0x_spin(Z_ab, rho_up, rho_dn, sigma_up, sigma_dn,
                           &q0xt, &dq0xt_drho_up, &dq0xt_drho_dn,
                           &dq0xt_dsigma_up, &dq0xt_dsigma_dn);
    double q0xt_ref = 4.144551809731131;
    double q0xt_err = fabs(q0xt - q0xt_ref);
    printf("q0x_spin %.15f err=%e\n", q0xt, q0xt_err);

    double q0xt_1, q0xt_2;
    vdwxc_compute_q0x_spin(Z_ab, rho_up - drho, rho_dn, sigma_up, sigma_dn,
                           &q0xt_1, &tmp, &tmp, &tmp, &tmp);
    vdwxc_compute_q0x_spin(Z_ab, rho_up + drho, rho_dn, sigma_up, sigma_dn,
                           &q0xt_2, &tmp, &tmp, &tmp, &tmp);
    double dq0xt_drho_up_numeric = 0.5 * (q0xt_2 - q0xt_1) / drho;
    err = fabs(dq0xt_drho_up_numeric - dq0xt_drho_up);
    printf("dq0xtdrho_up=%f, err=%e\n", dq0xt_drho_up, err);
    assert(q0xt_err < 1e-14);
    assert(err < 1e-7);

    vdwxc_compute_q0x_spin(Z_ab, rho_up, rho_dn - drho, sigma_up, sigma_dn,
                           &q0xt_1, &tmp, &tmp, &tmp, &tmp);
    vdwxc_compute_q0x_spin(Z_ab, rho_up, rho_dn + drho, sigma_up, sigma_dn,
                           &q0xt_2, &tmp, &tmp, &tmp, &tmp);
    double dq0xt_drho_dn_numeric = 0.5 * (q0xt_2 - q0xt_1) / drho;
    err = fabs(dq0xt_drho_dn_numeric - dq0xt_drho_dn);
    printf("dq0xtdrho_dn=%f, err=%e\n", dq0xt_drho_dn, err);
    assert(err < 1e-7);

    vdwxc_compute_q0x_spin(Z_ab, rho_up, rho_dn, sigma_up - dsigma, sigma_dn,
                           &q0xt_1, &tmp, &tmp, &tmp, &tmp);
    vdwxc_compute_q0x_spin(Z_ab, rho_up, rho_dn, sigma_up + dsigma, sigma_dn,
                           &q0xt_2, &tmp, &tmp, &tmp, &tmp);
    double dq0xt_dsigma_up_numeric = 0.5 * (q0xt_2 - q0xt_1) / dsigma;
    err = fabs(dq0xt_dsigma_up_numeric - dq0xt_dsigma_up);
    printf("dq0xt_dsigma_up %f, err=%e\n", dq0xt_dsigma_up, err);
    assert(err < 5e-8);

    vdwxc_compute_q0x_spin(Z_ab, rho_up, rho_dn, sigma_up, sigma_dn - dsigma,
                           &q0xt_1, &tmp, &tmp, &tmp, &tmp);
    vdwxc_compute_q0x_spin(Z_ab, rho_up, rho_dn, sigma_up, sigma_dn + dsigma,
                           &q0xt_2, &tmp, &tmp, &tmp, &tmp);
    double dq0xt_dsigma_dn_numeric = 0.5 * (q0xt_2 - q0xt_1) / dsigma;
    err = fabs(dq0xt_dsigma_dn_numeric - dq0xt_dsigma_dn);
    printf("dq0xt_dsigma_dn %f, err=%e\n\n", dq0xt_dsigma_dn, err);
    assert(err < 1e-7);

    double oldq0, olddq0_drho, olddq0_dsigma;
    double newq0, newdq0_drho, newdq0_dsigma;
    vdwxc_calculate_q0(1, Z_ab, 5.0, &rho, &sigma,
                       &oldq0, &olddq0_drho, &olddq0_dsigma);
    vdwxc_calculate_q0_nospin(1, Z_ab, &rho, &sigma,
                              &newq0, &newdq0_drho, &newdq0_dsigma);

    double dq0drho_up_spinpair, dq0drho_dn_spinpair;
    double dq0dsigma_up_spinpair, dq0dsigma_dn_spinpair;
    double q0_spinpair;

    vdwxc_compute_q0_spin(Z_ab, rho / 2., rho / 2.,
                          sigma / 4., sigma / 4.,
                          &q0_spinpair,
                          &dq0drho_up_spinpair, &dq0drho_dn_spinpair,
                          &dq0dsigma_up_spinpair, &dq0dsigma_dn_spinpair);

    printf("q0 spin-paired old/new comparison\n");
    printf("q0 old %.15e\n", oldq0);
    printf("q0 new %.15e\n", newq0);
    printf("q0 spp %.15e\n", q0_spinpair);
    printf("old dq0dn %.15e\n", olddq0_drho);
    printf("new dq0dn %.15e\n", newdq0_drho);
    printf("spp dq0dn %.15e %.15e\n", dq0drho_up_spinpair, dq0drho_dn_spinpair);
    printf("old dq0dsigma %.15e\n", olddq0_dsigma);
    printf("new dq0dsigma %.15e\n", newdq0_dsigma);
    printf("spp dq0dsigma %.15e %.15e\n\n", dq0dsigma_up_spinpair, dq0dsigma_dn_spinpair);

    // And now the big one!
    double dq0drho_up, dq0drho_dn;
    double dq0dsigma_up, dq0dsigma_dn;
    double q0ref = 4.372399873725762;
    vdwxc_compute_q0_spin(Z_ab,
                          rho_up, rho_dn,
                          sigma_up, sigma_dn,
                          &q0,
                          &dq0drho_up, &dq0drho_dn,
                          &dq0dsigma_up, &dq0dsigma_dn);
    dq0drho_up /= rho;
    dq0drho_dn /= rho;
    dq0dsigma_up /= rho;
    dq0dsigma_dn /= rho;
    double q0err = fabs(q0ref - q0);

    vdwxc_compute_q0_spin(Z_ab, rho_up - drho, rho_dn, sigma_up, sigma_dn,
                          &q0_1, &tmp, &tmp, &tmp, &tmp);
    vdwxc_compute_q0_spin(Z_ab, rho_up + drho, rho_dn, sigma_up, sigma_dn,
                          &q0_2, &tmp, &tmp, &tmp, &tmp);
    err = fabs(0.5 * (q0_2 - q0_1) / drho - dq0drho_up);
    printf("q0_spin\nq0 %.15f err %e\n", q0, q0err);
    printf("dq0drho_up=%f err=%e\n", dq0drho_up, err);
    assert(q0err < 1e-14);
    assert(err < 5e-8);

    vdwxc_compute_q0_spin(Z_ab, rho_up, rho_dn - drho, sigma_up, sigma_dn,
                          &q0_1, &tmp, &tmp, &tmp, &tmp);
    vdwxc_compute_q0_spin(Z_ab, rho_up, rho_dn + drho, sigma_up, sigma_dn,
                          &q0_2, &tmp, &tmp, &tmp, &tmp);
    err = fabs(0.5 * (q0_2 - q0_1) / drho - dq0drho_dn);
    printf("dq0drho_dn=%f err=%e\n", dq0drho_dn, err);
    assert(err < 1e-7);

    vdwxc_compute_q0_spin(Z_ab, rho_up, rho_dn, sigma_up - dsigma, sigma_dn,
                          &q0_1, &tmp, &tmp, &tmp, &tmp);
    vdwxc_compute_q0_spin(Z_ab, rho_up, rho_dn, sigma_up + dsigma, sigma_dn,
                          &q0_2, &tmp, &tmp, &tmp, &tmp);
    err = fabs(0.5 * (q0_2 - q0_1) / dsigma - dq0dsigma_up);
    printf("dq0dsigma_up=%f err=%e\n", dq0dsigma_up, err);
    assert(err < 1e-7);

    vdwxc_compute_q0_spin(Z_ab, rho_up, rho_dn, sigma_up, sigma_dn - dsigma,
                          &q0_1, &tmp, &tmp, &tmp, &tmp);
    vdwxc_compute_q0_spin(Z_ab, rho_up, rho_dn, sigma_up, sigma_dn + dsigma,
                          &q0_2, &tmp, &tmp, &tmp, &tmp);
    err = fabs(0.5 * (q0_2 - q0_1) / dsigma - dq0dsigma_dn);
    printf("dq0dsigma_dn=%f err=%e\n", dq0dsigma_dn, err);
    assert(err < 1e-7);

    double test_q0 = 4.4;
    double test_dq0 = 1e-8;
    double hq0, hq0_1, hq0_2;
    double dhq0dq;
    double qcut = 5.0;
    vdwxc_hfilter(test_q0, qcut, &hq0, &dhq0dq);
    vdwxc_hfilter(test_q0 - test_dq0, qcut, &hq0_1, &tmp);
    vdwxc_hfilter(test_q0 + test_dq0, qcut, &hq0_2, &tmp);
    double dhq0dq_numerical = 0.5 * (hq0_2 - hq0_1) / test_dq0;
    double dhq0dq_err = fabs(dhq0dq_numerical - dhq0dq);
    printf("hq0 %f, dhq0dq %f, numeric %f, err %e\n",
           hq0, dhq0dq, dhq0dq_numerical, dhq0dq_err);
    assert(dhq0dq_err < 2e-8);
    return 0;
}
