#include <stdio.h>
#include <assert.h>
#include <math.h>

#include "vdwxc.h"

#if defined HAVE_CONFIG_H
#include "config.h"
#endif

#include "vdw_core.h"


int main(int argc, char** argv)
{
    /*
    double rho_g = 1.2;
    double sigma_g = 0.7;
    double eps_g = 0.0;
    double dedrho_g = 0.0;
    double dedsigma_g = 0.0;

    vdwxc_cx_semilocal_exchange(1, &rho_g, &sigma_g, &eps_g, &dedrho_g, &dedsigma_g);
    double epsref = -0.94280203021413;
    double dedrhoref = -1.04533767734898;
    double dedsigmaref = -0.00142722900003;
    double epserr = fabs(eps_g - epsref);
    double dedrhoerr = fabs(dedrho_g - dedrhoref);
    double dedsigmaerr = fabs(dedsigma_g - dedsigmaref);
    printf("cx exchange\n");
    printf("res %18.14f %18.14f %18.14f\n", eps_g, dedrho_g, dedsigma_g);
    printf("err %18e %18e %18e\n", epserr, dedrhoerr, dedsigmaerr);
    assert(epserr < 2e-14);
    assert(dedrhoerr < 2e-14);
    assert(dedsigmaerr < 2e-14);

    eps_g = 0.0;
    dedrho_g = 0.0;
    vdwxc_lda_correlation(1, &rho_g, &eps_g, &dedrho_g);
    printf("lda correlation\n");
    printf("res %18.14f %18.14f\n", eps_g, dedrho_g);
    double epsref_lda = -0.08725425857624;
    double dedrhoref_lda = -0.08103610573413;
    double epserr_lda = fabs(eps_g - epsref_lda);
    double dedrhoerr_lda = fabs(dedrho_g - dedrhoref_lda);
    printf("err %18e %18e\n", epserr_lda, dedrhoerr_lda);
    assert(epserr_lda < 2e-14);
    assert(dedrhoerr_lda < 2e-14);
    return 0;*/
}
