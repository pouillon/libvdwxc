#include <math.h>
#include <stdio.h>
#include <assert.h>

#if defined HAVE_CONFIG_H
#include "config.h"
#endif

#include "vdw_core.h"
#include "vdw_q0.h"

// 1e-15 corresponds to kF~1e-5 which is the end of the standard q0 grid.
// However realistic values of q0 are more like 1e-3 to 5.
// libxc uses 1e-10 while QE uses 1e-12.  Let us use 1e-12 then for
// consistency.
#define RHOCUT (1e-12)


void vdwxc_calculate_q0(int N, double Z_ab, double q_cut,
                        double* rho, double* sigma,
                        double* q0, double* rho_dq0drho,
                        double* rho_dq0dsigma)
{
    int i;
    for (i=0; i<N; i++) {
        double mrho = *rho;// * rhofactor;
        double msigma = *sigma; //) * rhofactor;

        if (mrho < RHOCUT) {
            // TEMP FIX:
            // for small densities we should in fact use q_cut.
            // These things will be property of kernel later - probably.
            mrho = RHOCUT;
            *q0 = q_cut;
            *rho_dq0dsigma = 0.0;
            *rho_dq0drho = 0.0;
            rho++;
            sigma++;
            q0++;
            rho_dq0drho++;
            rho_dq0dsigma++;
            continue;
            //if (*rho<0)
            //   printf("Warning, negative density %.15f\n", *rho);
        }

        // kF = (3.0 * np.pi**2 * total_rho)**(1.0 / 3.0)
        double rho1_root3 = pow(mrho, 1.0 / 3.0);
        double kF = VDWXC_KF_PREFACTOR * rho1_root3;

        // r_s = (3.0 / (4.0 * np.pi * total_rho))**(1.0 / 3.0)
        double rs = VDWXC_RS_PREFACTOR / rho1_root3;
        double rs_sqr = rs*rs;
        double rs_root2 = sqrt(rs);
        double rho_sqr = mrho*mrho;

        double gradient_correction = -Z_ab / (36.0 * kF * rho_sqr) * msigma;
        double LDA_1 = 8.0 * VDWXC_PI / 3.0 * (VDWXC_LDA_A * (1.0 + VDWXC_LDA_a1 * rs));
        double LDA_2 = 2.0 * VDWXC_LDA_A * (VDWXC_LDA_b1 * rs_root2 + VDWXC_LDA_b2 * rs + \
                                          VDWXC_LDA_b3 * rs * rs_root2 + VDWXC_LDA_b4 * rs_sqr);

        double q = kF + LDA_1 * log(1.0 + 1.0 / LDA_2) + gradient_correction;
        int m;
        double exponent = 0.0;
        double dq0_dq = 0.0;
        double qscaled0 = q / q_cut;
        double qscaled = 1.0;

        for (m=1; m<=12; m++) {
            dq0_dq += qscaled;
            qscaled *= qscaled0;
            exponent += qscaled / m;
        }

        //printf("exponent %.15f", exponent);
        //printf("dq0_dq %.15f", dq0_dq);

        //mask = total_rho >= epsr
        //q0[mask] = (q_cut * (1.0 - np.exp(-exponent)))[mask]
        //dq0_dq *= np.exp(-exponent)
        // Ignore mask for now: DOUBLE CHECK THIS XXX
        dq0_dq *= exp(-exponent);
        *q0 = q_cut * (1.0 - exp(-exponent));

        *rho_dq0drho = dq0_dq * (kF / 3.0 - 7.0 / 3.0 * gradient_correction
                                 - 8.0 * VDWXC_PI / 9.0 * VDWXC_LDA_A * VDWXC_LDA_a1 * rs
                                 * log(1.0 + 1.0 / LDA_2)
                                 + LDA_1 / (LDA_2 * (1.0 + LDA_2))
                                 * (2.0 * VDWXC_LDA_A * (VDWXC_LDA_b1 / 6.0 * rs_root2
                                                       + VDWXC_LDA_b2 / 3.0 * rs
                                                       + VDWXC_LDA_b3 / 2.0 * rs * rs_root2
                                                       + 2.0 * VDWXC_LDA_b4 / 3.0 * rs_sqr)));

        *rho_dq0dsigma = -dq0_dq * Z_ab / (36.0 * kF * mrho);

        rho++;
        sigma++;
        q0++;
        rho_dq0drho++;
        rho_dq0dsigma++;
    }
}


// Perdew-wang G[A, alpha1, beta1, ..., beta4](rs) function
void vdwxc_G_pw(double A, double a1,
                double b1, double b2, double b3, double b4,
                double rootrs, double *e, double *dedrs)
{
    double rs = rootrs * rootrs;
    double rootrs3 = rootrs * rs;
    double rs2 = rootrs * rootrs3;
    double g0 = -2.0 * A * (1.0 + a1 * rs);
    double g1 = 2.0 * A * (b1 * rootrs + b2 * rs
                           + b3 * rootrs3 + b4 * rs2);
    *e = g0 * log(1.0 + 1.0 / g1);
    double dg1drs = A * (b1 / rootrs + 2.0 * b2 +
                         rootrs * (3.0 * b3 + 4.0 * b4 * rootrs));
    *dedrs = -2.0 * A * a1 * (*e) / g0 - g0 * dg1drs / (g1 * (1.0 + g1));
}


void vdwxc_G_zeta0(double rootrs, double *e, double *dedn)
{
    vdwxc_G_pw(0.031091, 0.21370, 7.5957, 3.5876, 1.6382, 0.49294,
               rootrs, e, dedn);
}

void vdwxc_G_zeta1(double rootrs, double *e, double *dedn)
{
    vdwxc_G_pw(0.015545, 0.20548, 14.1189, 6.1977, 3.3662, 0.62517,
         rootrs, e, dedn);
}

void vdwxc_G_alphac(double rootrs, double *e, double *dedn)
{
    vdwxc_G_pw(0.016887, 0.11125, 10.357, 3.6231, 0.88026, 0.49671,
               rootrs, e, dedn);
    (*e) *= -1.0;
    (*dedn) *= -1.0;
}


void vdwxc_compute_lda(double n, double *e, double *v)
{
    double rs = VDWXC_RS_PREFACTOR / pow(n, 1.0 / 3.0);
    double rootrs = sqrt(rs);
    vdwxc_G_zeta0(rootrs, e, v);
    // e is now energy density epsilon, and v is derivative wrt. rs.
    // convert to actual energy/particle and its derivative:
    *v = (*e) - rs * (*v) / 3.0;
    //*e *= n;
}

void vdwxc_compute_lda_spin_zeta(double n, double zeta,
                                 double *e, double *v_up, double *v_dn)
{
    //double C0I = 0.238732414637843;
    //double C1 = -0.45816529328314287;
    double CC1 = 1.9236610509315362;
    double CC2 = 2.5648814012420482;
    double IF2 = 0.58482236226346462;

    double rs = VDWXC_RS_PREFACTOR / pow(n, 1.0 / 3.0);
    double rootrs = sqrt(rs);

    double e_zeta0, dedrs_zeta0;
    vdwxc_G_zeta0(rootrs, &e_zeta0, &dedrs_zeta0);
    double e_zeta1, dedrs_zeta1;
    vdwxc_G_zeta1(rootrs, &e_zeta1, &dedrs_zeta1);
    double alpha, dalphadrs;
    vdwxc_G_alphac(rootrs, &alpha, &dalphadrs);

    double zp = 1.0 + zeta;
    double zm = 1.0 - zeta;
    double xp = pow(zp, 1.0 / 3.0);
    double xm = pow(zm, 1.0 / 3.0);
    double f = CC1 * (zp * xp + zm * xm - 2.0);
    double f1 = CC2 * (xp - xm);
    double zeta3 = zeta * zeta * zeta;
    double zeta4 = zeta3 * zeta;
    double x = 1.0 - zeta4;

    double decdrs = (dedrs_zeta0 * (1.0 - f * zeta4) +
                     dedrs_zeta1 * f * zeta4 +
                     dalphadrs * f * x * IF2);
    //printf("decdrs %f\n", decdrs);
    double decdzeta = (4.0 * zeta3 * f * (e_zeta1 - e_zeta0 - alpha * IF2) +
                       f1 * (zeta4 * e_zeta1 - zeta4 * e_zeta0
                             + x * alpha * IF2));
    //printf("decdzeta %f\n", decdzeta);
    //printf("f %f alpha %f %f\n", f, alpha, e1 - ec0);
    *e = e_zeta0 + f * (alpha * IF2 * x + (e_zeta1 - e_zeta0) * zeta4);
    *v_up = (*e) - rs * decdrs / 3.0 - (zeta - 1.0) * decdzeta;
    *v_dn = (*e) - rs * decdrs / 3.0 - (zeta + 1.0) * decdzeta;
    //*e *= n;
}

void vdwxc_compute_lda_spin(double n_up, double n_dn,
                            double *e, double *v_up, double *v_dn)
{
    double n = n_up + n_dn;
    double zeta = (n_up - n_dn) / n;
    vdwxc_compute_lda_spin_zeta(n, zeta, e, v_up, v_dn);
}

void vdwxc_compute_q0x(double Z_ab, double rho, double sigma, double *q0x,
                       double *dq0xdrho, double *dq0xdsigma)
{
    double rho_root3 = pow(rho, 1.0 / 3.0);
    double rho2 = rho * rho;
    double kF = VDWXC_KF_PREFACTOR * rho_root3;
    double _36_kF_rho2 = 36.0 * kF * rho2;

    *q0x = kF - Z_ab * sigma / _36_kF_rho2;
    *dq0xdrho = VDWXC_KF_PREFACTOR / (3.0 * rho_root3 * rho_root3) +
        7.0 * Z_ab * sigma / (3.0 * _36_kF_rho2 * rho);
    *dq0xdsigma = -Z_ab / _36_kF_rho2;
}

void vdwxc_compute_q0x_spin(double Z_ab, double rho_up, double rho_dn,
                            double sigma_up, double sigma_dn,
                            double *q0xt,
                            double *dq0xtdrho_up, double *dq0xtdrho_dn,
                            double *dq0xtdsigma_up, double *dq0xtdsigma_dn)
{

    double rho = rho_up + rho_dn;

    double q0x_2up, q0x_2dn;
    double dq0xdrho_2up, dq0xdrho_2dn;
    double dq0xdsigma_2up, dq0xdsigma_2dn;


    if(rho_up > RHOCUT / 2) {
        vdwxc_compute_q0x(Z_ab, 2.0 * rho_up, 4.0 * sigma_up,
                          &q0x_2up, &dq0xdrho_2up, &dq0xdsigma_2up);
    } else {
        q0x_2up = 0.0;
        dq0xdrho_2up = 0.0;
        dq0xdsigma_2up = 0.0;
    }
    if(rho_dn > RHOCUT / 2) {
        vdwxc_compute_q0x(Z_ab, 2.0 * rho_dn, 4.0 * sigma_dn,
                          &q0x_2dn, &dq0xdrho_2dn, &dq0xdsigma_2dn);
    } else {
        q0x_2dn = 0.0;
        dq0xdrho_2dn = 0.0;
        dq0xdsigma_2dn = 0.0;
    }

    // QE 'saturates' the q0x variables as well for each spin.
    // This code will enable
    // the QE behaviour (for testing):
    /* double qcut = 5.0;
    double qcutbig = 4.0 * qcut;
    double hderiv;

    vdwxc_hfilter(q0x_2up, qcutbig, &q0x_2up, &hderiv);
    dq0xdrho_2up *= hderiv;
    dq0xdsigma_2up *= hderiv;

    vdwxc_hfilter(q0x_2dn, qcutbig, &q0x_2dn, &hderiv);
    dq0xdrho_2dn *= hderiv;
    dq0xdsigma_2dn *= hderiv;*/
    // done with silly extra cutoff

    *q0xt = (rho_up * q0x_2up + rho_dn * q0x_2dn) / rho;
    *dq0xtdrho_up = (rho_dn * (q0x_2up - q0x_2dn)
                     + 2.0 * rho_up * rho * dq0xdrho_2up) / (rho * rho);
    *dq0xtdrho_dn = (rho_up * (q0x_2dn - q0x_2up)
                     + 2.0 * rho_dn * rho * dq0xdrho_2dn) / (rho * rho);
    *dq0xtdsigma_up = 4.0 * rho_up / rho * dq0xdsigma_2up;
    *dq0xtdsigma_dn = 4.0 * rho_dn / rho * dq0xdsigma_2dn;
}


void vdwxc_hfilter(double q0, double qcut, double *hq0, double *dhq0dq0)
{
    //*dhq0dq0 = 999.;
    //return;  // XXXXXXXXXXXXXXXXXXX
    int m;
    double qrel = q0 / qcut;
    double qrel_power = 1.0;
    double exponent = 0.0, exponent_deriv = 0.0;
    for (m=1; m<=12; m++) {
        exponent_deriv += qrel_power;
        qrel_power *= qrel;
        exponent += qrel_power / m;
    }
    double exp_exponent = exp(-exponent);
    *hq0 = qcut * (1.0 - exp_exponent);
    *dhq0dq0 = exp_exponent * exponent_deriv;
}

#define LDA_VOLUME_FACTOR (-4.0 * VDWXC_PI / 3.0)

void vdwxc_compute_q0_nospin(double Z_ab,
                             double rho,
                             double sigma,
                             double* q0,
                             double* dq0drho,
                             double* dq0dsigma)
{
    double qcut = 5.0;
    if(rho < RHOCUT) {
        // Temp fix for small rho
        *q0 = qcut;
        *dq0drho = 0.0;
        *dq0dsigma = 0.0;
        // XXX update
        return;
    }

    vdwxc_compute_q0x(Z_ab, rho, sigma, q0, dq0drho, dq0dsigma);
    *dq0drho *= rho;
    *dq0dsigma *= rho;

    double e_lda;
    double v_lda;
    vdwxc_compute_lda(rho, &e_lda, &v_lda);
    *q0 += LDA_VOLUME_FACTOR * e_lda;
    // XXX slightly better to calculate and represent rho * dq0drho
    *dq0drho += LDA_VOLUME_FACTOR * (v_lda - e_lda);// / rho;
    double dhq0dq0;
    vdwxc_hfilter(*q0, qcut, q0, &dhq0dq0);
    *dq0drho *= dhq0dq0;
    *dq0dsigma *= dhq0dq0;
}

void vdwxc_compute_q0_spin(double Z_ab,
                           double rho_up, double rho_dn,
                           double sigma_up, double sigma_dn,
                           double* q0,
                           double* rho_dq0drho_up, double* rho_dq0drho_dn,
                           double* rho_dq0dsigma_up, double* rho_dq0dsigma_dn)
{
    double qcut = 5.0;

    if(rho_up + rho_dn < RHOCUT) {
        *q0 = qcut;
        *rho_dq0drho_up = 0.0;
        *rho_dq0drho_dn = 0.0;
        *rho_dq0drho_up = 0.0;
        *rho_dq0drho_dn = 0.0;
        return;
    }

    rho_up = fmax(rho_up, RHOCUT / 2.0);
    rho_dn = fmax(rho_dn, RHOCUT / 2.0);
    double rho = rho_up + rho_dn;

    vdwxc_compute_q0x_spin(Z_ab, rho_up, rho_dn,
                           sigma_up, sigma_dn,
                           q0,
                           rho_dq0drho_up, rho_dq0drho_dn,
                           rho_dq0dsigma_up, rho_dq0dsigma_dn);
    *rho_dq0drho_up *= rho;
    *rho_dq0drho_dn *= rho;
    *rho_dq0dsigma_up *= rho;
    *rho_dq0dsigma_dn *= rho;

    double e_lda;
    double v_lda_up, v_lda_dn;
    vdwxc_compute_lda_spin(rho_up, rho_dn,
                           &e_lda, &v_lda_up, &v_lda_dn);
    *q0 += LDA_VOLUME_FACTOR * e_lda;
    // XXX slightly better to calculate and represent rho * dq0drho
    *rho_dq0drho_up += LDA_VOLUME_FACTOR * (v_lda_up - e_lda);
    *rho_dq0drho_dn += LDA_VOLUME_FACTOR * (v_lda_dn - e_lda);

    double dhq0dq0;
    vdwxc_hfilter(*q0, qcut, q0, &dhq0dq0);
    *rho_dq0drho_up *= dhq0dq0;
    *rho_dq0drho_dn *= dhq0dq0;
    *rho_dq0dsigma_up *= dhq0dq0;
    *rho_dq0dsigma_dn *= dhq0dq0;
}


void vdwxc_calculate_q0_nospin(int N, double Z_ab,
                               double *rho_g,
                               double *sigma_g,
                               double *q0_g,
                               double *rho_dq0drho_g,
                               double *rho_dq0dsigma_g)
{
    int i;
    for (i=0; i<N; i++) {
        vdwxc_compute_q0_nospin(Z_ab, rho_g[i],
                                sigma_g[i],
                                &q0_g[i],
                                &rho_dq0drho_g[i],
                                &rho_dq0dsigma_g[i]);
    }
}



void vdwxc_calculate_q0_spin(int N, double Z_ab,
                             double *rho_up_g, double *rho_dn_g,
                             double *sigma_up_g, double *sigma_dn_g,
                             double *q0_g,
                             double *rho_dq0drho_sg,
                             double *rho_dq0dsigma_sg)
{
    int i;
    for (i=0; i<N; i++) {
        vdwxc_compute_q0_spin(Z_ab, rho_up_g[i], rho_dn_g[i],
                              sigma_up_g[i], sigma_dn_g[i],
                              &q0_g[i],
                              &rho_dq0drho_sg[i], &rho_dq0drho_sg[i + N],
                              &rho_dq0dsigma_sg[i], &rho_dq0dsigma_sg[i + N]);
    }
}
