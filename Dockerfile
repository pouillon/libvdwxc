# Base image
FROM ubuntu:18.04

# Install required packages
RUN apt-get update -qy
RUN apt-get upgrade -qy
RUN apt-get install -qy \
                    build-essential \
                    libtool \
                    fftw3-dev \
                    dh-autoreconf \
                    python3-pip
RUN pip3 install --upgrade \
                 pip \
                 setuptools \
		 sphinx \
                 sphinxcontrib-bibtex \
                 sphinx_rtd_theme \
                 sphinx_sitemap