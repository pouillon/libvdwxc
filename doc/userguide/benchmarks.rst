.. _benchmarks:
.. index:: Benchmarks

Numerical benchmarks
************************************

We calculate the dispersive-interactions standard test set, S22
:cite:`JurSpoCer06`. We use the SG15 pseusopotentials :cite:`Ham13`,
with extremely tight grid spacing of 0.12 Å and a large vacuum of 7.5
Å.
