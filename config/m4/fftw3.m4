# -*- Autoconf -*-
#
# Copyright (C) 2015 Yann Pouillon
#
# This file is part of the Libvdwxc software package. For license information,
# please see the COPYING file in the top-level directory of the Libvdwxc source
# distribution.
#



# VDW_FFTW3_DETECT()
# ------------------
#
# Check whether the FFTW3 library is working.
#
AC_DEFUN([VDW_FFTW3_DETECT],[
  dnl Init
  vdw_fftw3_ok="unknown"
  vdw_fftw3_has_hdrs="unknown"
  vdw_fftw3_has_libs="unknown"

  dnl Backup environment
  vdw_saved_CPPFLAGS="${CPPFLAGS}"
  vdw_saved_LIBS="${LIBS}"

  dnl Prepare build parameters
  CPPFLAGS="${CPPFLAGS} ${vdw_fftw3_incs}"
  LIBS="${vdw_fftw3_libs} ${LIBS}"

  dnl Look for C includes
  AC_LANG_PUSH([C])
  AC_CHECK_HEADERS([fftw3.h],
    [vdw_fftw3_has_hdrs="serial"], [vdw_fftw3_has_hdrs="no"])
  if test "${vdw_fftw3_has_hdrs}" = "serial"; then
    if test "${enable_mpi}" = "yes"; then
      AC_CHECK_HEADERS([fftw3-mpi.h],
        [vdw_fftw3_has_hdrs="yes"], [vdw_fftw3_has_hdrs="no"])
    else
      vdw_fftw3_has_hdrs="yes"
    fi
  fi
  AC_LANG_POP([C])

  dnl Look for C libraries and routines
  if test "${vdw_fftw3_has_hdrs}" = "yes"; then
    AC_LANG_PUSH([C])
    AC_MSG_CHECKING([whether the serial FFTW3 libraries work])
    AC_LINK_IFELSE([AC_LANG_PROGRAM(
      [[
#include <fftw3.h>
      ]],
      [[
        fftw_plan *plan;
        fftw_complex *a1, *a2;
        fftw_execute_dft(plan, a1, a2);
      ]])], [vdw_fftw3_has_libs="yes"], [vdw_fftw3_has_libs="no"])
    AC_MSG_RESULT([${vdw_fftw3_has_libs}])
    if test "${vdw_fftw3_has_libs}" = "yes"; then
      if test "${enable_mpi}" = "yes"; then
        AC_MSG_CHECKING([whether the MPI FFTW3 libraries work])
        AC_LINK_IFELSE([AC_LANG_PROGRAM(
          [[
#include <fftw3-mpi.h>
          ]],
          [[
            fftw_mpi_init();
          ]])], [vdw_fftw3_has_libs="yes"], [vdw_fftw3_has_libs="no"])
        AC_MSG_RESULT([${vdw_fftw3_has_libs}])
      fi
    fi
    AC_LANG_POP([C])
  fi

  dnl Take final decision
  AC_MSG_CHECKING([whether we have a full FFTW3 support])
  if test "${vdw_fftw3_has_hdrs}" = "yes" -a \
          "${vdw_fftw3_has_libs}" = "yes"; then
    vdw_fftw3_ok="yes"
  else
    vdw_fftw3_ok="no"
  fi
  AC_MSG_RESULT([${vdw_fftw3_ok}])

  dnl Restore environment
  CPPFLAGS="${vdw_saved_CPPFLAGS}"
  LIBS="${vdw_saved_LIBS}"
]) # VDW_FFTW3_DETECT
